
// Playing Cards
// Devon Lozier

#include <iostream>
#include <conio.h>

using namespace std;



enum class Rank
{
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE,
	//JOKER should perhaps be included
};

enum class Suit
{
	SPADES,
	HEARTS,
	DIAMONDS,
	CLUBS
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{


	(void)_getch();
	return 0;
}
